#
#   ██████╗ ██████╗ ███╗   ███╗██████╗ ██████╗ ███████╗██╗  ██╗███████╗███╗   ██╗███████╗██╗ ██████╗ ███╗   ██╗
#  ██╔════╝██╔═══██╗████╗ ████║██╔══██╗██╔══██╗██╔════╝██║  ██║██╔════╝████╗  ██║██╔════╝██║██╔═══██╗████╗  ██║
#  ██║     ██║   ██║██╔████╔██║██████╔╝██████╔╝█████╗  ███████║█████╗  ██╔██╗ ██║███████╗██║██║   ██║██╔██╗ ██║
#  ██║     ██║   ██║██║╚██╔╝██║██╔═══╝ ██╔══██╗██╔══╝  ██╔══██║██╔══╝  ██║╚██╗██║╚════██║██║██║   ██║██║╚██╗██║
#  ╚██████╗╚██████╔╝██║ ╚═╝ ██║██║     ██║  ██║███████╗██║  ██║███████╗██║ ╚████║███████║██║╚██████╔╝██║ ╚████║
#   ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝
#

'''

    Comprehension is used to manipulate iterables based on the information their elements hold.
    To filter elements out of an iterable based on their position inside the iterable, see the splicing block.

    Below examples can be found.

    Please do keep in mind that the examples use lists,
    but encapsulating the comprehension inside tuple() or any other iterable will work.

'''

def generic():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  list generated from list comprehension
    lst_comp = [number for number in lst]

    #  The "for number in lst" section of the comprehension iterates through lst like one would in a for loop
    #  putting number in front of the iterative part of the comprehension simply inserts the number that is being considered into the new list
    #  thus the above code copies lst into lst_comp

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_comp: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    return lst, "[number for number in lst]", lst_comp



def generic_static():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  list generated from list comprehension
    lst_comp = [0 for number in lst]

    #  By putting a 0 in front of number, we ensure that lst_comp is filled with 0s and has the same length as lst
    #  0 can be replaced with any item of data
    #  however a quicker way of doing this is to simply do as follows

    lst_mult = [0] * len(lst)

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_comp: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    #  lst_mult: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    return lst, "[number for number in lst]", lst_comp, lst_mult



def elaborated():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


    #  list generated from list comprehension
    lst_comp = [number + 3 for number in lst]

    #  The number before the iterative part of the comprehension can be elaborated upon
    #  Here 3 is added to every number in lst, then stored in lst_comp
    #  Any elaboration can be done to number
    #  this makes list comprehension a useful tool to modify all the items in a list in a simple manner

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_comp: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    return lst, "[number + 3 for number in lst]", lst_comp



def nested():

    #  List to sample from
    lst = [0, 1, 2, 3, 4]

    #  list generated from list comprehension
    lst_comp = [[digit for digit in lst] for number in lst]

    #  Comprehension can be nested
    #  as can be seen in the example above, the list is copied into lst_comp for every item it has
    #  this is a perfect way to make matrices

    #  output:
    #  lst: [0, 1, 2, 3, 4]
    #  lst_comp: [[0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4]]

    return lst, "[[digit for digit in lst] for number in lst]", lst_comp



def nested_elaboration():

    #  List to sample from
    lst = [0, 1, 2, 3, 4]

    #  lists generated from list comprehension
    lst_comp = [[digit - 2 for digit in lst] for number in lst]

    #  By combining nested comprehension and elaboration, we can elaborate the items anywhere in newly formed list

    #  output:
    #  lst: [0, 1, 2, 3, 4]
    #  lst_comp: [[-2, -1, 0, 1, 2], [-2, -1, 0, 1, 2], [-2, -1, 0, 1, 2], [-2, -1, 0, 1, 2], [-2, -1, 0, 1, 2]]

    return lst, "[[digit for digit in lst] for number in lst]", lst_comp


def conditional():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  list generated from list comprehension
    lst_comp0 = [number for number in lst if number%2 == 0]

    #  By appending a condition to the list comprehension, the values that are put into the new list are filtered
    #  In this example the number is inserted into the new list only if its divisible by 2
    #  Naturally any statement of truth can be put instead of "number%2 == 0"
    #  See the above code as follows:

        #  lst_comp1 = []
        #  for number in lst:
        #      if number % 2 == 0:
        #          lst_comp1.append("even")

    #  list generated from list comprehension
    lst_comp1 = ["even" if number%2 == 0 else "odd" for number in lst]

    #  Another variation can be seen above
    #  "even" is inserted if the statement is true, else "odd" is inserted
    #  While the syntax might be slightly less intuitive, it is very powerful
    #  See the above code as follows:

        #  lst_comp1 = []
        #  for number in lst:
        #      if number % 2 == 0:
        #          lst_comp1.append("even")
        #      else:
        #          lst_comp1.append("odd")

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_comp0: [0, 2, 4, 6, 8]
    #  lst_comp1: ['even', 'odd', 'even', 'odd', 'even', 'odd', 'even', 'odd', 'even', 'odd']

    return lst, "[number for number in lst if number%2 == 0]", lst_comp0, "[\"even\" if number%2 == 0 else \"odd\" for number in lst]", lst_comp1



if __name__ == "__main__":

    #  Run this python script to see what the code does on your machine.

    gen_eg = generic()

    print("Generic Example: ")
    print(f"Original list: {gen_eg[0]}")
    print(f"Comprehension: {gen_eg[1]}")
    print(f"Comprehended list: {gen_eg[2]}", end="\n\n")


    gen_stat = generic_static()

    print("Generic Example 2: ")
    print(f"Original list: {gen_stat[0]}")
    print(f"Comprehension: {gen_stat[1]}")
    print(f"Comprehended list: {gen_stat[2]}")
    print(f"Multiplied list: {gen_stat[3]}", end="\n\n")


    elab_eg = elaborated()

    print("Elaboration Example: ")
    print(f"Original list: {elab_eg[0]}")
    print(f"Comprehension: {elab_eg[1]}")
    print(f"Comprehended list: {elab_eg[2]}", end="\n\n")


    nest_eg = nested()

    print("Nested Example: ")
    print(f"Original list: {nest_eg[0]}")
    print(f"Comprehension: {nest_eg[1]}")
    print(f"Comprehended list: {nest_eg[2]}", end="\n\n")


    nest_el = nested_elaboration()

    print("Nested and Elaborated Example: ")
    print(f"Original list: {nest_el[0]}")
    print(f"Comprehension: {nest_el[1]}")
    print(f"Comprehended list: {nest_el[2]}", end="\n\n")


    nest_cond = conditional()

    print("Conditional Example: ")
    print(f"Original list: {nest_cond[0]}")
    print(f"Comprehension: {nest_cond[1]}")
    print(f"Comprehended list 0: {nest_cond[2]}")
    print(f"Comprehension: {nest_cond[3]}")
    print(f"Comprehended list 1: {nest_cond[4]}", end="\n\n")
