#
#  ███████╗██████╗ ██╗     ██╗ ██████╗██╗███╗   ██╗ ██████╗
#  ██╔════╝██╔══██╗██║     ██║██╔════╝██║████╗  ██║██╔════╝
#  ███████╗██████╔╝██║     ██║██║     ██║██╔██╗ ██║██║  ███╗
#  ╚════██║██╔═══╝ ██║     ██║██║     ██║██║╚██╗██║██║   ██║
#  ███████║██║     ███████╗██║╚██████╗██║██║ ╚████║╚██████╔╝
#  ╚══════╝╚═╝     ╚══════╝╚═╝ ╚═════╝╚═╝╚═╝  ╚═══╝ ╚═════╝
#

'''

    Splicing in python is used to select certain parts of an iterable.

    The general syntax of slicing is as follows:

        list[a:b:c]

        list --> the list that is being spliced
        a --> the starting index (default: 0)
        b --> the end index (default: len(list))
        c --> the step (default: 1)

    The new list that the splice returns is equal contains all the elements from the original list,
    from the starting index (included) up to the end index (excluded).
    The step dictates with what frequency the range is sampled.
    For example, with a step of i, every i_th element in the considered range is inserted into the returned list.

    This way of manipulating lists filters out elements based on their index within the list.
    To manipulate and filter elements based on their values, see comprehension block.

    Below examples can be found.

'''


def everything():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  Spliced list
    lst_spl = lst[:]

    #  When the splicing is done as above, all the defaults are used
    #  This means that the entire original list is copied into lst_spl

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_spl: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    return lst, "lst[:]", lst_spl


def generic():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  Spliced list
    lst_spl = lst[2:6]

    #  In this example, the sample list is spliced from index 2 up to index 6 (index 6 not included)
    #  by default the step is 1

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_spl: [2, 3, 4, 5]

    return lst, "lst[2:6]", lst_spl


def no_start():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  Spliced list
    lst_spl = lst[:6]

    #  In this case, only the ending index is specified
    #  defaults are used for the unspecified values

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_spl: [0, 1, 2, 3, 4, 5]

    return lst, "lst[:6]", lst_spl

def no_end():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  Spliced list
    lst_spl = lst[2:]

    #  Similarly, in this case, only the ending index is specified
    #  defaults are used for the unspecified values

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_spl: [2, 3, 4, 5, 6, 7, 8, 9]

    return lst, "lst[2:]", lst_spl


def step_not_1():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  Spliced list
    lst_spl = lst[::2]

    #  In this example, with a step of 2, every 2nd element is spliced into lst_spl

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_spl: [0, 2, 4, 6, 8]

    return lst, "lst[::2]", lst_spl


def negative_step():

    #  List to sample from
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    #  Spliced list
    lst_spl = lst[::-1]

    #  In this example, with a step of -1, the list is iterated through backwards
    #  this results into lst_spl being lst backwards

    #  output:
    #  lst: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    #  lst_spl: [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]

    return lst, "lst[::-1]", lst_spl


if __name__ == "__main__":

    #  Run this python script to see what the code does on your machine.

    uni_eg = everything()

    print("All inclusive Example: ")
    print(f"Original list: {uni_eg[0]}")
    print(f"Comprehension: {uni_eg[1]}")
    print(f"Comprehended list: {uni_eg[2]}", end="\n\n")


    gen_eg = generic()

    print("Generic Example: ")
    print(f"Original list: {gen_eg[0]}")
    print(f"Comprehension: {gen_eg[1]}")
    print(f"Comprehended list: {gen_eg[2]}", end="\n\n")


    no_start_eg = no_start()

    print("No Starting Index Example: ")
    print(f"Original list: {no_start_eg[0]}")
    print(f"Comprehension: {no_start_eg[1]}")
    print(f"Comprehended list: {no_start_eg[2]}", end="\n\n")


    no_end_eg = no_end()

    print("No Ending Index Example: ")
    print(f"Original list: {no_end_eg[0]}")
    print(f"Comprehension: {no_end_eg[1]}")
    print(f"Comprehended list: {no_end_eg[2]}", end="\n\n")


    no_end_eg = no_end()

    print("No Ending Index Example: ")
    print(f"Original list: {no_end_eg[0]}")
    print(f"Comprehension: {no_end_eg[1]}")
    print(f"Comprehended list: {no_end_eg[2]}", end="\n\n")


    step_not_1_eg = step_not_1()

    print("Step != 1 Example: ")
    print(f"Original list: {step_not_1_eg[0]}")
    print(f"Comprehension: {step_not_1_eg[1]}")
    print(f"Comprehended list: {step_not_1_eg[2]}", end="\n\n")


    neg_stepi_eg = negative_step()

    print("Negative Step Example: ")
    print(f"Original list: {neg_stepi_eg[0]}")
    print(f"Comprehension: {neg_stepi_eg[1]}")
    print(f"Comprehended list: {neg_stepi_eg[2]}", end="\n\n")
