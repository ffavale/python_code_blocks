#
#  ███████╗████████╗██████╗ ██╗███╗   ██╗ ██████╗ ███████╗
#  ██╔════╝╚══██╔══╝██╔══██╗██║████╗  ██║██╔════╝ ██╔════╝
#  ███████╗   ██║   ██████╔╝██║██╔██╗ ██║██║  ███╗███████╗
#  ╚════██║   ██║   ██╔══██╗██║██║╚██╗██║██║   ██║╚════██║
#  ███████║   ██║   ██║  ██║██║██║ ╚████║╚██████╔╝███████║
#  ╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝
#

'''

    Strings are an ordered collection of charachters.

    Strings are iterable.
    This means they can be manipulated in interesting ways.

'''

def splicing():

    #  sample string
    sample = "this is a random sentence"

    #  string spliced from the sample
    str_spl = sample[3:12]

    #  Since strings are iterable, they can be spliced just like lists and tuples
    #  In this example the spliced string will be taken from the element in the 3rd index to the element in the 11th index of the sample

    return sample, "sample[3:12]", str_spl



def comprehension():

    #  sample string
    sample = "this is another random sentence"

    #  string spliced from the sample
    str_spl = "".join(char for char in sample if char in "aeiou")

    #  Strings can be iterated trough, thus they can be runthrough comprehension
    #  The resulting iterable can be "".join ed into another string

    return sample, '"".join(char for char in sample if char in "aeiou")', str_spl



def concatenation():

    #  sample string
    sample0 = "this is a random sentence"
    sample1 = "this is another random sentence"


    #  string concatenated from the samples
    str_cat = sample0 + "and" + sample1

    #  The resulting string is sample0 followed by sample1 with "and" in between
    #  Only strings can be concatenated, if a non string has to be concatenated, first it must be made into a string

    return sample0, sample1, 'sample0 + "and" + sample1', str_cat



'''

    f_strings are formatted strings.
    In short, f_strings are an easy way to manipulate strings and make them more useful

'''

def f_string_0():

    var1 = 0
    var2 = 2.5
    var3 = 'birds'
    lst1 = range(5)

    #  All of the above variables are inserted into fstr where the variable is inserted in the curly braces
    #  To denote an f_string, simply put quotation marks preceded by an f
    fstr = f"{var1} is the number 0, {var2} is a float, {var3} is a string and {lst1} is a list"

    #  backslashes cannot be put into f_strings, thus
    #      f"Newline = {ord('\n')}"
    #  while
    #      ord_nl = ord('\n')
    #      f"Newline = {ord_nl}"
    #  is valid

    return 'f"{var1} is the number 0, {var2} is a float, {var3} is a string and {lst1} is a list"', fstr



if __name__ == "__main__":

    #  Run this python script to see what the code does on your machine.

    str_spl = splicing()

    print("Splicing Example: ")
    print(f"Sample string: {str_spl[0]}")
    print(f"Splicing: {str_spl[1]}")
    print(f"Spliced string: {str_spl[2]}", end="\n\n")

    str_comp = comprehension()

    print("Comprehension Example: ")
    print(f"Sample string: {str_comp[0]}")
    print(f"Comprehension: {str_comp[0]}")
    print(f"Comprehended string: {str_comp[0]}", end="\n\n")

    str_cat = concatenation()

    print("Concatenation Example: ")
    print(f"Sample string: {str_cat[0]}")
    print(f"Concatenation: {str_cat[1]}")
    print(f"Concatenated string: {str_cat[2]}", end="\n\n")

    str_f = f_string_0()

    print("f_string Example: ")
    print(f"Formatting: {str_f[0]}")
    print(f"Formatted string: {str_f[1]}", end="\n\n")
