#
#  ██▀███  ▓█████  ▄████▄   █    ██  ██▀███    ██████  ██▓ ▒█████   ███▄    █
#  ▓██ ▒ ██▒▓█   ▀ ▒██▀ ▀█   ██  ▓██▒▓██ ▒ ██▒▒██    ▒ ▓██▒▒██▒  ██▒ ██ ▀█   █
#  ▓██ ░▄█ ▒▒███   ▒▓█    ▄ ▓██  ▒██░▓██ ░▄█ ▒░ ▓██▄   ▒██▒▒██░  ██▒▓██  ▀█ ██▒
#  ▒██▀▀█▄  ▒▓█  ▄ ▒▓▓▄ ▄██▒▓▓█  ░██░▒██▀▀█▄    ▒   ██▒░██░▒██   ██░▓██▒  ▐▌██▒
#  ░██▓ ▒██▒░▒████▒▒ ▓███▀ ░▒▒█████▓ ░██▓ ▒██▒▒██████▒▒░██░░ ████▓▒░▒██░   ▓██░
#  ░ ▒▓ ░▒▓░░░ ▒░ ░░ ░▒ ▒  ░░▒▓▒ ▒ ▒ ░ ▒▓ ░▒▓░▒ ▒▓▒ ▒ ░░▓  ░ ▒░▒░▒░ ░ ▒░   ▒ ▒
#    ░▒ ░ ▒░ ░ ░  ░  ░  ▒   ░░▒░ ░ ░   ░▒ ░ ▒░░ ░▒  ░ ░ ▒ ░  ░ ▒ ▒░ ░ ░░   ░ ▒░
#    ░░   ░    ░   ░         ░░░ ░ ░   ░░   ░ ░  ░  ░   ▒ ░░ ░ ░ ▒     ░   ░ ░
#     ░        ░  ░░ ░         ░        ░           ░   ░      ░ ░           ░
#                  ░

'''
Recursion is a way to handle the flow of a program.

It consits of a function calling itself after slightly altering the input.

This section describes the general structure of recursive flow and cool hacks to simplify recursive life.

===================

Generally, recursive functions have 3 parts:

    - The end condition
        this section checks if the recursive flow should end
        usually in the form of an if statement before the rest of the function with a hardcoded return value inside

    - The elaborative section
        in this section, the input of the function is changed or elaborated upon in some way
        everythingthat happens here happens on the way down the recursion "rabbit hole"

    - The return section
        this section often consits in "return {recursive_func}(elab)" (elab being the elborated input)
        it represents the way out of the recursion "rabbit hole"

'''

#  All the examples that follow use factorial as a case study because doing recursion raw is hell

def factorial(n: int) -> int:

    #  This is the end condition
    #  the function will stop calling itself if n = 0 and returns a hardcoded 1
    if n == 0:
        return 1

    #  This is the elaboration section
    #  n is elaborated to n2 by subtracting 1
    n2 = n-1

    #  Finally the return section
    #  here the function calls itself with the elaborated input
    return n * factorial(n2)


def factorial_wlst(n: int, results: list) -> int:
    #  A cool hack to know about lists and recusrion is that once a list is initialized and passed into functions, the local instances are just refencing the original instance of the list
    #  this means that if we want to have values persist through the recursiv flow, we just initialize an emty list outside the recursion
    #  and whenever it changes, it is changed everywhere

    #  using the same example as above...

    #  end codition
    if n == 0:
        return 1

    #  elaboration section
    n2 = n-1

    result = n * factorial_wlst(n2, results)

    results.append(result)

    #  return section
    return result

    #  to see this in action go to the following link:
    #  https://pythontutor.com/visualize.html#code=def%20factorial_wlst%28n%3A%20int,%20results%3A%20list%29%20-%3E%20int%3A%0A%0A%20%20%20%20%23%20%20end%20codition%0A%20%20%20%20if%20n%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%201%0A%0A%20%20%20%20%23%20%20elaboration%20section%0A%20%20%20%20n2%20%3D%20n-1%0A%0A%20%20%20%20result%20%3D%20n%20*%20factorial_wlst%28n2,%20results%29%0A%0A%20%20%20%20results.append%28result%29%0A%0A%20%20%20%20%23%20%20return%20section%0A%20%20%20%20return%20result%0A%0A%0A%0Ares_ex%20%3D%20%5B%5D%0A%0Aprint%28factorial_wlst%285,%20res_ex%29%29%0A%0Aprint%28res_ex%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false


'''
During the 28-01-2022 programming exam, a question was easily solvable by using nested for loops, but had to have recursion in it.

The following example shows how 2 nested for loops for combining lists can be implemented in a recursive manner.

'''

def combinator(lst1: list, lst2: list, index_1: int, index_2: int, results: list):
    """
    The function implements with recursion:

        for item_a in list1:
            for item_b in list2:
                results.append(item_a + item_b)
    """

    results.append(lst1[index_1] + lst2[index_2])

    if index_2 == len(lst2) - 1:

        if index_1 == len(lst1) - 1:

            return

        combinator(lst1, lst2, index_1 + 1, 0, results)
        return

    combinator(lst1, lst2, index_1, index_2 + 1, results)



if __name__ == "__main__":

    #  Run this python script to see what the code does on your machine.

    print("Factorial: ")
    print("Result:", factorial(10), end="\n\n")


    print("Factorial with persistent list:")

    res = []

    print("Result:", factorial_wlst(10, res))

    print("Persitent list:", res, end="\n\n")

    
    print("Recursive nested for loop:")

    lst1 = ["abc", "cde", "efg"]
    lst2 = ["123", "456", "789"]

    res = []

    combinator(lst1, lst2, 0, 0, res)
    
    print("List1:", lst1)
    print("List2:", lst2)
    print("Combination", res)
