# python_code_blocks

This is a collection of python code blocks, explanations, exercises and exam simulations that will come in handy for programming.

## How to use?

You can browse the files directly here on codeberg. However, to run the scripts and to experiment with them downloading the files is necssary.

The following are instructions to get the files onto your silicon.

### Install git

Git is a version control system.
In short, it gives you the most up to date version of stuff.

#### Windows

1. Go to [https://git-scm.com/download/win](https://git-scm.com/download/win) and download git
2. Run the installer
3. Click on next until you click on install
4. You're done

#### MacOS

1. Open the command prompt
2. Run this:
`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
3. Run the following command:
`brew install git`
4. You're done

#### Linux

1. It depends on your distro
2. If you can, try and find it in your graphical package manager
3. Consult the following link: [https://git-scm.com/download/linux](https://git-scm.com/download/linux)
4. You should be done. If not: Good luck, Have fun :)

### Clone the repository

Run
```sh
git clone https://codeberg.org/ffavale/python_code_blocks.git
```
in your shell when in the directory you would like the code to be saved to.

#### Windows:

Run
```sh
git clone https://codeberg.org/ffavale/python_code_blocks.git
```
in the cmd after moving to the directory you would like to save it.

### Update the repository

The python_code_blocks repository is undergoing development. This means that updates are frequent.
It is reccomended that you update the repository before every time you use it.

Run
```sh
git pull
```
in your shell when in the python_code_blocks directory.

## Want to help?

If you want to help with development, open an issue with something you wish to see added or contact us at python_code_blocks@studenti.uniroma1.it
